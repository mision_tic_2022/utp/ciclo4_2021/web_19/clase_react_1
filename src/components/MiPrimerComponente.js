import React, {Component} from "react";
/*
    Comentario de varias lineas
     */
    //comentario de una sola linea
class MiPrimerComponente extends Component{

    render(){
        return(
            <div>
                <h1>Mi Primer Componente</h1>
                <h2>Subtitulo del componente</h2>
            </div>            
        );
    }

}

export default MiPrimerComponente;