import React, {Component} from 'react';
import PersonForm from './PersonForm';
import PersonTable from './PersonTable';


class PersonApp extends Component{
    constructor(props){
        super(props);
        this.state = {
            people: []
        }
        this.addPerson = this.addPerson.bind(this);
    }
    
    addPerson(objPerson){
        //Laura Rodriguez
        this.setState({
            people: [...this.state.people, objPerson]
          })
    }
    
  

    render(){
        return(
            //Fragment-> Es una etiqueta vacia, se elimina al momento de renderizar los elementos en el DOM
            <>
                <h1>Registro Persona - Componente basado en clase</h1>   
                <PersonForm addPerson={this.addPerson} mensaje="Esto es un mensaje desde PersonApp"/> 
                <PersonTable people={this.state.people}/>            
            </>
        );
    }
}

export default PersonApp;