import React, { Component } from 'react';

const form = {
    name: "",
    lastname: "",
    phone: "",
    email: ""
}

export default class PersonForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: form
        }
        console.log("Props-> ", this.props.mensaje);
        //Biendear
        //this.handlerName = this.handlerName.bind(this);
        //this.handlerLastname = this.handlerLastname.bind(this);
        this.handlerValue = this.handlerValue.bind(this);
        this.handlerSubmit = this.handlerSubmit.bind(this);
    }

    //Manejador de eventos
    handlerName(e) {
        console.log("Capturando el evento");
        console.log("Evento--> ", e);
        console.log("Valor-> ", e.target.value);
        //Modificar el valor del estado nombre
        this.setState({
            name: e.target.value
        });
    }

    handlerValue(e){
        let form = this.state.form;
        this.setState({
            form: {...form, [e.target.name] : e.target.value }
        });
        //Laura Rodriguez
        /*
        this.setState({
            [e.target.name] : e.target.value
        })
        */
        /*
        switch(e.target.name){
            case "name": 
                this.setState({name: e.target.value});
                break;
            case "lastname": 
                this.setState({lastname: e.target.value});
                break;
            case "phone": 
                this.setState({phone: e.target.value});
                break;
            case "email": 
                this.setState({email: e.target.value});
                break;
            default:    
                break;
        }
        */
        console.log(this.state);
    }


    handlerSubmit(e){
        console.log(e);
        e.preventDefault();
        //Ejecutar función del padre
        let objPerson = this.state.form;
        this.props.addPerson(objPerson);
    }


    render() {
        return (
            <>
                <form onSubmit={this.handlerSubmit}>
                    <label htmlFor="name">Nombre: </label>
                    <input id="name" name="name" type="text"
                        value={this.state.form.name} onChange={this.handlerValue}
                        placeholder="Escribe tu nombre..." />
                    <label htmlFor="lastname">Apellido</label>
                    <input id="lastname" name="lastname" onChange={this.handlerValue}
                        value={this.state.form.lastname} placeholder="lastname" />
                    <br />
                    <br />
                    <label htmlFor="phone">Teléfono</label>
                    <input id="phone" name="phone" type="tel" onChange={this.handlerValue}
                        value={this.state.form.phone} placeholder="Escribe tu teléfono" />
                    <label htmlFor="email">Email</label>
                    <input id="email" name="email" type="email" onChange={this.handlerValue}
                        value={this.state.form.email} placeholder="Escribe tu email" />
                    <br/>
                    <br/>
                    <button type="submit">Guardar</button>
                </form>
            </>
        );
    }
}